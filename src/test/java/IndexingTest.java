import org.junit.Test;
import service.IndexingFacade;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class IndexingTest {

    private static final String DATA_FILE="src/test/resources/directoryTest/test1.txt";
    private static final String DATA_DIRECTORY="src/test/resources/directoryTest";
    private static final String DIRECTORY_FILES="src\\test\\resources\\directoryTest\\test1.txt" +
                                                "\n" + "src\\test\\resources\\directoryTest\\test2.txt";
    private static final String WORD="amine";
    private static final String WRONG_WORD="mark";


    @Test
    public void FileWithWordTest(){

        String output = "";
        IndexingFacade indexingFacade = new IndexingFacade(DATA_FILE,WORD);
        output = indexingFacade.search();
        assertThat(output,is(DATA_FILE));
    }

    @Test
    public void FileNotContainsWordTest(){

        String output = "";
        IndexingFacade indexingFacade = new IndexingFacade(DATA_FILE,WRONG_WORD);
        output = indexingFacade.search();
        assertThat(output,is(""));
    }

    @Test
    public void DirectoryWithWordTest(){

        String output = "";
        IndexingFacade indexingFacade = new IndexingFacade(DATA_DIRECTORY,WORD);
        output = indexingFacade.search();
        assertThat(output,is(DIRECTORY_FILES));
    }

    @Test
    public void DirectoryNotContainsWordTest(){

        String output = "";
        IndexingFacade indexingFacade = new IndexingFacade(DATA_DIRECTORY,WRONG_WORD);
        output = indexingFacade.search();
        assertThat(output,is(""));
    }
}
