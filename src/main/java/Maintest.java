
import service.IndexingFacade;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Maintest {

    public static void main(String[] args) {

        String path = "";
        String word = "";
        String output = "";
        do {
            System.out.println("Please enter your file/directory path : ");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));


            try {
                path = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Please enter a word : ");


            try {
                word = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }while(path.equals("") || word.equals(""));

        IndexingFacade indexingFacade = new IndexingFacade(path, word);
        output = indexingFacade.search();

        try {
            if(output.equals("")){
                System.out.println("No file contains this word");
            }
            System.out.println("List of files contains the given word (" + word + ") : \n" + output);

            }catch(NullPointerException e){
                System.out.println();
            }

    }
}
