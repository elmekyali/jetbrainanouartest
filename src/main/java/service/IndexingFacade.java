package service;


import service.strategy.IndexingDirectoryStrategy;
import service.strategy.IndexingFileStrategy;
import service.strategy.IndexingStrategy;

import java.io.File;
import java.util.List;

public class IndexingFacade {

    private final String path;
    private final String word;
    private IndexingStrategy indexingStrategy;

    public IndexingFacade(String path, String word) {
        this.path = path;
        this.word = word;
        this.indexingStrategy = getIndexingStrategy(path);
    }

    private IndexingStrategy getIndexingStrategy(String path) {

        File file = new File(path);

        if (file.isDirectory()) {
            return new IndexingDirectoryStrategy(path);
        }
        if (file.isFile()) {
            return new IndexingFileStrategy(path);
        }
        return null;

    }

    public String search() {


        if(indexingStrategy == null){
            System.out.println("No file or directory with this path");
            return null;
        }
        List<String> listFiles = indexingStrategy.findFilesWithWord(word);

        String output = String.join("\n", listFiles);
        return output;

    }
}
