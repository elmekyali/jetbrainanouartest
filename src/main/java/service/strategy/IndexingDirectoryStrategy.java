package service.strategy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class IndexingDirectoryStrategy extends IndexingStrategy {

    private final String path;

    public IndexingDirectoryStrategy(String path) {
        this.path=path;
    }

    @Override
    public List<String> findFilesWithWord(String word) {

        List<String> result = new ArrayList<>();
        try {
            result = Files.list(Paths.get(path))
                    .filter(file-> containsWord(word,file.toString()))
                    .map(file-> file.toString())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
