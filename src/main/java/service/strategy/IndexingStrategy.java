package service.strategy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public abstract class IndexingStrategy {


    public abstract List<String> findFilesWithWord(String word);

    protected boolean containsWord(String word,String path){

        boolean exist = false;

        try (Stream<String> stream = Files.lines(Paths.get(path))) {
            exist = stream
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .anyMatch(s -> s.equalsIgnoreCase(word));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return exist;
    }
}
