package service.strategy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class IndexingFileStrategy extends IndexingStrategy {

    private final String path;

    public IndexingFileStrategy(String path) {
        this.path=path;
    }

    @Override
    public List<String> findFilesWithWord(String word) {

        return containsWord(word,path)? Arrays.asList(path) : Collections.emptyList();
    }
}
